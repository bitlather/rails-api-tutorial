class SerializableFruitCultivar < JSONAPI::Serializable::Resource
  # For more information, see http://jsonapi-rb.org/guides/serialization/defining.html
  type 'fruit_cultivar'

  belongs_to :fruit

  attributes :name, :fruit_id

end
