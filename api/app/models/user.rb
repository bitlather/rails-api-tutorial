class User < ApplicationRecord

  has_secure_password

  validates_uniqueness_of :email, :case_sensitive => false

  # Is email a valid address? (uses app/validators/email_validator.rb)
  validates :email, presence: true, email: true

  # Is password strong enough? (uses app/validators/password_validator.rb)
  validates :password, password: true, :on => :create
  
  validates_length_of :first_name, :within => 1..100
  validates_length_of :last_name, :within => 1..100
  validates_length_of :email, :within => 5..200

  before_save :make_email_lowercase, :validate_role

  # Set default values in model if they're empty
  after_initialize :init_set_default_values

  ROLES = {
    super_admin: 'Super Administrator',
    admin: 'Administrator',
    user: 'User'
  }.freeze

  # Define methods like is_super_admin? for each role
  User::ROLES.each do |role, _label|
    define_method "is_#{role}?" do
      self.role == role.to_s
    end
  end

  private

    def init_set_default_values
      # Set default values if empty:
      self.role ||= 'user'
    end

    def make_email_lowercase
      self.email.downcase!
    end

    def validate_role
      # The to_sym is important. For example, 'user' is not the same as :user.
      raise "Role not recognized: #{self.role}" unless ROLES.key?(self.role.to_sym)
    end

end
