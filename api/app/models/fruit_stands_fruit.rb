class FruitStandsFruit < ApplicationRecord

  #
  # Because of the following belongs_to lines, we can write:
  #
  #    fruit_stands_fruit = FruitStandsFruit.new(
  #      fruit_stand: FruitStand.find(params[:id]),
  #      fruit: Fruit.find(add_fruit_params[:fruit_id])
  #    )
  #
  # Without them, we would have to write the slightly more cumbersome:
  #
  #    fruit_stands_fruit = FruitStandsFruit.new(
  #      fruit_stand_id: FruitStand.find(params[:id]).id,
  #      fruit_id: Fruit.find(add_fruit_params[:fruit_id]).id
  #    )
  #
  belongs_to :fruit_stand
  belongs_to :fruit

  #
  # This improves error handling when someone tries to create duplicate copies
  # of the same fruit_id/fruit_stand_id.
  #
  validates_uniqueness_of :fruit_stand_id, :scope => :fruit_id

end
