module Api
  module V1
    class SuperAdminAbility

      include CanCan::Ability

      def initialize(user)
        #
        # There are no limitations on super admins.
        #
        can :manage, :all
      end

    end
  end
end
