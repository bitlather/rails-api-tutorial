class PasswordValidator < ActiveModel::EachValidator

  HAS_DIGIT_FORMAT = /\A
    (?=.*\d)           # Must contain a digit
  /x

  HAS_SYMBOL_FORMAT = /\A
    (?=.*[[:^alnum:]]) # Must contain a symbol
  /x

  MIN_PASSWORD_LENGTH = 6
  MAX_PASSWORD_LENGTH = 72 # This value was defined by something under the hood, so let's just roll with it

  def validate_each(record, attribute, value)
    unless !value.nil? && value.length >= MIN_PASSWORD_LENGTH && value.length <= MAX_PASSWORD_LENGTH
      record.errors[attribute] << (options[:message] || "length must be between #{MIN_PASSWORD_LENGTH} and #{MAX_PASSWORD_LENGTH} characters long")
    end

    unless value =~ HAS_DIGIT_FORMAT
      record.errors[attribute] << (options[:message] || "must contain a number (0-9)")
    end

    unless value =~ HAS_SYMBOL_FORMAT
      record.errors[attribute] << (options[:message] || "must contain a symbol (!@$ etc)")
    end
  end
end
