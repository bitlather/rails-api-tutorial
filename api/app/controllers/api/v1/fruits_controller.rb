module Api
  module V1
    class FruitsController < BaseController

      # We're phasing out the old authenticate_request in favor of cancan.
      # Once weve fully implemented cancan in all the controllers, we'll just
      # delete authenticate_request and remove this skip.
      skip_before_action :authenticate_request

      # Move user authentication and authorization to cancan.
      load_and_authorize_resource :only => [:create, :index, :show, :update]

      # Don't load resource on destroy or else we could get 404s on duplicate
      # requests to delete the same row.
      authorize_resource :only => [:destroy]

      # Handle 404s.
      rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response

      def create
       
        fruit = Fruit.new(
          name: create_params['name'],
        )

        if fruit.save

          render nothing: true, status: :no_content

          #
          # You could also do the following, but symbols like :no_content are preferred:
          #
          #   status: 204
          #
          # See list here:
          # http://billpatrianakos.me/blog/2013/10/13/list-of-rails-status-code-symbols/
          #

        else

          render_jsonapi_errors(fruit)

        end
      end

      def destroy

        # Oops we didn't do this correctly before, so we're fixing it now.

        #
        # No need to throw 404 if fruit does not exist, since user just wants this
        # information gone. So we don't use Fruit.find() because this would throw
        # a ActiveRecord::RecordNotFound exception and cause the request to
        # return a 404.
        #
        # Also note we have to use "destroy_all" instead of "delete_all".
        #
        #     delete_all just deletes the fruit records.
        #
        #     destroy_all deletes all records that are linked to the fruit too,
        #     as is defined by relationships defined in models, such as
        #     has_and_belongs_to_many.
        #
        Fruit.where(
          id: params[:id]
        ).destroy_all

        #
        # From https://stackoverflow.com/questions/6439416/deleting-a-resource-using-http-delete
        #
        # "If you DELETE something that doesn't exist, you should just return a 204 (even if the
        # resource never existed). The client wanted the resource gone and it is gone. Returning
        # a 404 is exposing internal processing that is unimportant to the client and will
        # result in an unnecessary error condition."
        #
        render nothing: true, status: :no_content

      end

      def index

        #
        # Because of sorting and whatnot, we're not going to use any magic variables provided by
        # cancan's load_and_authorize_resource.
        #
        # But, load_and_authorize_resource did provide @fruits, which we could use. That's for
        # another day, though.
        #
        fruits = sorted_fruits || Fruit.all
        render jsonapi: fruits, include: include_from_params, status: :ok

      end

      def show

        #
        # load_and_authorize_resource automatically creates the @fruit variable for us.
        #
        render jsonapi: @fruit, include: include_from_params, status: :ok

      end

      def update

        #
        # load_and_authorize_resource automatically creates the @fruit variable for us.
        #

        @fruit.name = update_params['name'];

        if @fruit.save

          render nothing: true, status: :no_content

        else

          render_jsonapi_errors(@fruit)

        end

      end

      private

        def create_params
          params.require(:fruit)
                .permit(
                  :name
                )
        end

        def include_from_params
          return [] unless params[:include].present?

          results = []

          #
          # Wrap include param in commas to make finding a string easier.
          #
          includes = ",#{params[:include]},"

          #
          # Push supported resources to the array if they are in the 'include' param.
          #
          results.push(:fruit_cultivars) if includes.include? ",fruit_cultivars," # Must be plural because Fruit model has_many :fruit_cultivars

          #
          # Return an array of things to include.
          #
          results

        end

        def sorted_fruits
          return unless params[:sort].present?
          
          order = params[:sort].casecmp?('desc') ? :desc : :asc
          fruits = Fruit.order(name: order)
        end

        def update_params
          params.require(:fruit)
                .permit(
                  :name
                )
        end

    end
  end
end
