module Api
  module V1
    class PingController < BaseController

      # We're phasing out the old authenticate_request in favor of cancan.
      # Once weve fully implemented cancan in all the controllers, we'll just
      # delete authenticate_request and remove this skip.
      skip_before_action :authenticate_request

      # Just ignore CanCan for this resource.
      skip_authorization_check

      def show
        render_json_success(:data, { ping: 'pong' })
      end
    end
  end
end
