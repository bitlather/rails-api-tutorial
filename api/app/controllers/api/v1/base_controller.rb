module Api
  module V1
    class BaseController < ApplicationController

      rescue_from CanCan::AccessDenied do |exception|

        render json: {
          errors: [
            {
              status: '403',
              title: "Access Denied",
              detail: exception.message
            }
          ]
        }, status: :forbidden

      end

      def render_not_found_response(exception)
        # Error format from https://jsonapi.org/examples/#error-objects-basics
        render json: {
          errors: [
            {
              status: '404',
              title: "Record not found",
              detail: exception.message
            }
          ]
        }, status: :not_found
      end

    end
  end
end
