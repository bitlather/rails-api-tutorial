module Api
  module V1
    class UsersController < BaseController

      #
      # Creating a user or confirming their email address does not require user
      # to already be logged in.
      #
      skip_before_action :authenticate_request, only: [:confirm_email, :create]
      # Just ignore CanCan for now, until we can implement it later:
      skip_authorization_check :only => [:confirm_email, :create]
      rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response

      def confirm_email

        user = User.find(params[:id])

        if user.email_confirmation_code == ''

          #
          # Just return success if user was already confirmed.
          #
          # The user wants to be confirmed, they are confirmed, so there's no
          # need to throw an error.
          #
          render nothing: true, status: :no_content

        elsif user.email_confirmation_code == params[:code]

          user.email_confirmation_code = ''
          user.save!
          render nothing: true, status: :no_content

        else

          render json: { errors: [ { detail: 'Code did not match' } ] }, status: :unprocessable_entity

        end
      end

      def create

        #
        # Create a confirmation code with no vowels so we don't accidentally
        # spell a cuss word. Might be better to put this in the user model,
        # but this works for now.
        #
        # Note the difference:
        #
        #    (0..7) means 0,1,2,3,4,5,6,7 ("eight times" or "0 up to and including 7")
        #    (0...7) means 0,1,2,3,4,5,6 ("seven times" or "0 up to 7")
        #
        confirmation_code_characters = 'BCDFGHJKLMNPQRSTVWXZbcdfghjklmnpqrstvwxz'
        confirmation_code = (0..7).map { confirmation_code_characters[rand(confirmation_code_characters.length)] }.join

        user = User.new(
          first_name: create_params['first_name'],
          last_name: create_params['last_name'],
          email: create_params['email'],
          email_confirmation_code: confirmation_code,
          password: create_params['password']
        )

        if user.save

          if Rails.env.development?
            puts "Send an email to '#{user.email}' to confirm their email address with confirmation code (#{user.email_confirmation_code})."
          end

          # We're not sending an email .... yet.

          render nothing: true, status: :no_content

          #
          # You could also do the following, but symbols like :no_content are preferred:
          #
          #   status: 204
          #
          # See list here:
          # http://billpatrianakos.me/blog/2013/10/13/list-of-rails-status-code-symbols/
          #

        else

          render_jsonapi_errors(user)

        end

      end

      private

        def create_params
          params.require(:user)
                .permit(
                  :first_name,
                  :last_name,
                  :email,
                  :password
                )
        end

    end
  end
end