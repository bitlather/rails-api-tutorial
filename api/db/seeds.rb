# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
def run
    create_fruits
    create_fruit_stands
end

def create_fruits
    @apple = FactoryBot.create(:fruit, name: 'Apple')
    FactoryBot.create(:fruit, name: 'Banana')
    orange = FactoryBot.create(:fruit, name: 'Orange')    

    FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Fuji')
    FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Gala')
    FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Pink Lady')

    FactoryBot.create(:fruit_cultivar, fruit: orange, name: 'Blood Orange')
    FactoryBot.create(:fruit_cultivar, fruit: orange, name: 'Tangor')
end

def create_fruit_stands
    ollies_fruit_stand = FactoryBot.create(:fruit_stand, name: "Ollie's Orchard")
    ollies_fruit_stand.fruits << @apple
end

#
# Only run the seeder on development environment.
#
if !Rails.env.development?

    #
    # You can test this case with $ rake db:seed RAILS_ENV=test
    #
    puts
    puts "You may only run the seeder on the development environment."
    puts

else

    #
    # Delete data.
    #
    Fruit.destroy_all
    FruitStand.destroy_all

    #
    # Run the seeder.
    #
    run

end
