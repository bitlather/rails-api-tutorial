class CreateFruitCultivars < ActiveRecord::Migration[6.0]
  def change
    create_table :fruit_cultivars do |t|
      t.string :name
      t.integer :fruit_id, :unsigned => true

      t.timestamps
    end

    #
    # Dual-key unique constraint on fruit and its cultivar name.
    # Because it's dual-key, you could have a "gala" apple and a "gala" orange.
    #    
    add_index :fruit_cultivars, [:fruit_id, :name], :unique => true

    #
    # The fruit_id should reference the fruit record.
    # Because of naming conventions, this knows to link
    # fruit_cultivars.fruit_id to fruits.id.
    #
    add_foreign_key :fruit_cultivars, :fruits
    #
    # You could also specify the column, if the naming convention doesn't match
    # up correctly for magic to handle it:
    #
    #    add_foreign_key :fruit_cultivars, :fruits, column: :fruit_id
    #

  end
end
