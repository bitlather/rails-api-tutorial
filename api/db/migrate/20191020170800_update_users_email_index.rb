class UpdateUsersEmailIndex < ActiveRecord::Migration[6.0]
  def up
    #
    # We want to change an index on the users table, but we've already released
    # the users table migration. So, we have to create a new migration to change
    # the index.
    #
    #
    # The previous index did not work with SQLite, and only allowed the creation
    # of one user. So, we'll drop the index and create a new unique index on the
    # email column.
    #
    # This isn't ideal; even though the model checks for a unique email without
    # being case-sensitive, a race condition (in an albeit unlikely scenario)
    # could allow users with "bob@me.com" and "BOB@me.com" to be created.
    #
    # By forcing the email address to be lower case in the model, this mitigates
    # the issues faced by this application - but, if any other applications use
    # this database, then the error is still possible.
    #
    # We're calling this "good enough" for now. It seems that creating
    # case-insensitive string column indexes is a little different for different
    # databases (eg MySQL vs PostgreSQL).
    #
    # Also note that the following failed:
    #
    #     remove_index :users, :name => "index_users_on_lower_email_index", :unique => true
    #
    # I also noticed that a program for querying SQLite databases crashed whenever
    # I tried to look at the indexes on the users table. So, we're going to do a
    # naughty and just comment out the index in the original migration.
    #
    # We probably wouldn't be using SQLite on a production environment anyway, and
    # I think a better database application, like MySQL or PostgreSQL, would not
    # have let us get into this conundrum in the first place.
    #
    # I also could've tested by creating more than one user. RSPEC deletes all users
    # between tests and no test existed that used more than one user, so the bug
    # slipped past me.
    #
    add_index :users, [:email], :name => "index_users_on_email_index", :unique => true
  end
end
