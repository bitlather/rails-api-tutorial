class UpdateUsersAddRole < ActiveRecord::Migration[6.0]
  def change
    #
    # We want to add a column to the users table, but we've already released
    # the users table migration. So, we have to create a new migration to add
    # the column.
    #
    change_table :users do |t|
      t.string :role
    end
  end
end
