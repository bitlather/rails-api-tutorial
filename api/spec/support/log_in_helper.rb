module LogInHelpers
  def log_in_as_admin
    user = FactoryBot.create(:admin)
    auth_header_for_user(user)
    user
  end

  def log_in_as_super_admin
    user = FactoryBot.create(:super_admin)
    auth_header_for_user(user)
    user
  end

  def log_in_as_unauthorized_admin
    user = FactoryBot.create(:admin)
    header 'Authorization', "Bearer ZZZ"
    user
  end

  def log_in_as_unconfirmed_admin
    user = FactoryBot.create(:admin, email_confirmation_code: 'UNCONFRM')
    auth_header_for_user(user)
    user
  end

  def log_in_as_user
    user = FactoryBot.create(:user)
    auth_header_for_user(user)
    user
  end
end