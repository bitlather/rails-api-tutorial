require 'rails_helper'
require 'rspec_api_documentation/dsl'

#
# This file is basically permissions testing against all user types.
#
# We don't want this in the acceptance folder, because this would
# generate documentation bloat.
#

resource 'FruitStand' do

  before do

    header 'Content-Type', 'application/json'

    @apple = FactoryBot.create(:fruit, name: 'Apple')
    @fuji = FactoryBot.create(:fruit_cultivar, fruit: @apple, name: 'Fuji')
    @stand = FactoryBot.create(:fruit_stand, name: 'Apples R Us')
    @stand.fruits << @apple

  end

  delete '/api/v1/fruit_stands/:id' do

    let(:id) { @stand.id }

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Delete (204)' do
        expect(status).to eq(204)
        expect(FruitStandsFruit.all.size).to eq(0)
        expect(FruitStand.all.size).to eq(0)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'guest' do
      example_request 'Delete (403)' do
        expect(status).to eq(403)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Delete (204)' do
        expect(status).to eq(204)
        expect(FruitStandsFruit.all.size).to eq(0)
        expect(FruitStand.all.size).to eq(0)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Delete (401)' do
        expect(status).to eq(401)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Delete (403)' do
        expect(status).to eq(403)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

  end

  delete '/api/v1/fruit_stands/:id/fruits/:fruit_id' do

    let(:id) { @stand.id }

    let(:fruit_id) { @apple.id }

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Remove Fruit (204)' do
        expect(status).to eq(204)
        expect(FruitStandsFruit.all.size).to eq(0)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'guest' do
      example_request 'Remove Fruit (403)' do
        expect(status).to eq(403)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Remove Fruit (204)' do
        expect(status).to eq(204)
        expect(FruitStandsFruit.all.size).to eq(0)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Remove Fruit (401)' do
        expect(status).to eq(401)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Remove Fruit (403)' do
        expect(status).to eq(403)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

  end

  get '/api/v1/fruit_stands/:id' do

    let(:id) { @stand.id }

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Show (200)' do
        expect(status).to eq(200)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'guest' do
      example_request 'Show (200)' do
        expect(status).to eq(200)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Show (200)' do
        expect(status).to eq(200)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Show (401)' do
        expect(status).to eq(401)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Show (200)' do
        expect(status).to eq(200)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

  end

  get '/api/v1/fruit_stands' do

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Index (200)' do
        expect(status).to eq(200)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'guest' do
      example_request 'Index (200)' do
        expect(status).to eq(200)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Index (200)' do
        expect(status).to eq(200)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Index (401)' do
        expect(status).to eq(401)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Index (200)' do
        expect(status).to eq(200)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

  end

  post '/api/v1/fruit_stands/:id/fruits' do

    let(:id) { @stand.id }

    let(:raw_post) do
      {
        fruit_stands_fruit: {
          fruit_id: FactoryBot.create(:fruit, name: 'Orange').id
        }
      }.to_json
    end

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Add Fruit (204)' do
        expect(status).to eq(204)
        expect(FruitStandsFruit.all.size).to eq(2)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(2)
      end
    end

    describe 'guest' do
      example_request 'Add Fruit (403)' do
        expect(status).to eq(403)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(2)
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Add Fruit (204)' do
        expect(status).to eq(204)
        expect(FruitStandsFruit.all.size).to eq(2)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(2)
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Add Fruit (401)' do
        expect(status).to eq(401)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(2)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Add Fruit (403)' do
        expect(status).to eq(403)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(2)
      end
    end

  end

  post '/api/v1/fruit_stands' do

    let(:raw_post) do
      {
        fruit_stand: {
          name: "Ollie's Orchard"
        }
      }.to_json
    end

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Create (204)' do
        expect(status).to eq(204)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(2)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'guest' do
      example_request 'Create (403)' do
        expect(status).to eq(403)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Create (204)' do
        expect(status).to eq(204)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(2)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Create (401)' do
        expect(status).to eq(401)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Create (403)' do
        expect(status).to eq(403)
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

  end

  put '/api/v1/fruit_stands/:id' do
    let(:id) { @stand.id }
    let(:raw_post) do
      {
        fruit_stand: {
          name: 'Bananas R Us'
        }
      }.to_json
    end

    describe 'admin' do
      before { log_in_as_admin }
      example_request 'Update (204)' do
        expect(status).to eq(204)
        expect(FruitStand.first.name).to eq('Bananas R Us')
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'guest' do
      example_request 'Update (403)' do
        expect(status).to eq(403)
        expect(FruitStand.first.name).to eq('Apples R Us')
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'super_admin' do
      before { log_in_as_super_admin }
      example_request 'Update (204)' do
        expect(status).to eq(204)
        expect(FruitStand.first.name).to eq('Bananas R Us')
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'unauthorized_admin' do
      before { log_in_as_unauthorized_admin }
      example_request 'Update (401)' do
        expect(status).to eq(401)
        expect(FruitStand.first.name).to eq('Apples R Us')
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

    describe 'user' do
      before { log_in_as_user }
      example_request 'Update (403)' do
        expect(status).to eq(403)
        expect(FruitStand.first.name).to eq('Apples R Us')
        expect(FruitStandsFruit.all.size).to eq(1)
        expect(FruitStand.all.size).to eq(1)
        expect(FruitCultivar.all.size).to eq(1)
        expect(Fruit.all.size).to eq(1)
      end
    end

  end

end
