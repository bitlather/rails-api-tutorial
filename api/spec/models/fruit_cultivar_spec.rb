require 'rails_helper'

RSpec.describe FruitCultivar, type: :model do

  it "can't have the same name for the same fruit" do

    apple = FactoryBot.create(:fruit, name: 'Apple')

    FactoryBot.create(:fruit_cultivar, fruit: apple, name: 'Fuji')
    expect{FactoryBot.create(:fruit_cultivar, fruit: apple, name: 'fUJI')}.to raise_error(ActiveRecord::RecordInvalid)

  end

  it "is able to have the same cultivar name for different fruits" do

    apple = FactoryBot.create(:fruit, name: 'Apple')
    orange = FactoryBot.create(:fruit, name: 'Orange')

    FactoryBot.create(:fruit_cultivar, name: 'Fuji', fruit: apple)
    FactoryBot.create(:fruit_cultivar, name: 'Fuji', fruit: orange)

  end

  it "must have a fruit ID" do

    expect{FactoryBot.create(:fruit_cultivar, name: 'Fuji')}.to raise_error(ActiveRecord::RecordInvalid)

  end

  it "must have a fruit ID for a fruit that exists" do

    expect{FactoryBot.create(:fruit_cultivar, fruit_id: 999, name: 'Fuji')}.to raise_error(ActiveRecord::RecordInvalid)

  end

  it "must have a name" do

    apple = FactoryBot.create(:fruit, name: 'Apple')

    expect{FactoryBot.create(:fruit_cultivar, fruit: apple, name: '')}.to raise_error(ActiveRecord::RecordInvalid)

  end

  it "must not have a long name" do

    apple = FactoryBot.create(:fruit, name: 'Apple')

    expect{FactoryBot.create(:fruit_cultivar, fruit: apple, name: Array.new(101){ 'a' }.join)}.to raise_error(ActiveRecord::RecordInvalid)

  end

end
