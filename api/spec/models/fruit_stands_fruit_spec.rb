require 'rails_helper'

RSpec.describe FruitStandsFruit, type: :model do

  it "can exist" do

    fruits_galore = FactoryBot.create(:fruit_stand, name: 'Fruits Galore')
    apple = FactoryBot.create(:fruit, name: 'apple')
    orange = FactoryBot.create(:fruit, name: 'orange')
    fruits_galore.fruits << apple # Add and save apple
    fruits_galore.fruits << orange

    expect(FruitStandsFruit.all.size).to eq 2

  end

  it "cannot have duplicates" do

    fruits_galore = FactoryBot.create(:fruit_stand, name: 'Fruits Galore')
    apple = FactoryBot.create(:fruit, name: 'apple')

    FactoryBot.create(:fruit_stands_fruit, fruit_id: apple.id, fruit_stand_id: fruits_galore.id)
    expect{FactoryBot.create(:fruit_stands_fruit, fruit_id: apple.id, fruit_stand_id: fruits_galore.id)}.to raise_error(ActiveRecord::RecordInvalid)
    #
    # Note that the expectation above required curly braces. If you use
    # parentheses instead, the test will fail.
    #
    
  end

  it "is deleted when you delete its fruit stand" do

    fruits_galore = FactoryBot.create(:fruit_stand, name: 'Fruits Galore')
    apple = FactoryBot.create(:fruit, name: 'apple')
    fruits_galore.fruits << apple # Add and save apple

    expect(FruitStandsFruit.all.size).to eq 1

    fruits_galore.destroy

    expect(FruitStandsFruit.all.size).to eq 0
    expect(FruitStand.all.size).to eq 0
    expect(Fruit.all.size).to eq 1
    
  end

  it "is deleted when you delete its fruit" do

    fruits_galore = FactoryBot.create(:fruit_stand, name: 'Fruits Galore')
    apple = FactoryBot.create(:fruit, name: 'apple')
    fruits_galore.fruits << apple # Add and save apple

    expect(FruitStandsFruit.all.size).to eq 1

    apple.destroy

    expect(FruitStandsFruit.all.size).to eq 0
    expect(FruitStand.all.size).to eq 1
    expect(Fruit.all.size).to eq 0
    
  end

end
