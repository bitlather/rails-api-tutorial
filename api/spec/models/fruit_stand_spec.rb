require 'rails_helper'

RSpec.describe FruitStand, type: :model do

  it "can't have the same name" do

    FactoryBot.create(:fruit_stand, name: 'Fruits Galore')
    expect{FactoryBot.create(:fruit_stand, name: 'fRUITS gALORE')}.to raise_error(ActiveRecord::RecordInvalid)

  end

  it "must have a name" do

    expect{FactoryBot.create(:fruit_stand, name: '')}.to raise_error(ActiveRecord::RecordInvalid)

  end

  it "must not have a long name" do

    expect{FactoryBot.create(:fruit_stand, name: Array.new(101){ 'a' }.join)}.to raise_error(ActiveRecord::RecordInvalid)

  end

end
