require 'rails_helper'

RSpec.describe User, type: :model do

  it "can check user role is admin" do

    user = FactoryBot.create(:user, role: 'admin')

    expect(user.is_admin?).to eq(true)
    expect(user.is_super_admin?).to eq(false)
    expect(user.is_user?).to eq(false)

  end

  it "can check user role is super_admin" do

    user = FactoryBot.create(:user, role: 'super_admin')

    expect(user.is_admin?).to eq(false)
    expect(user.is_super_admin?).to eq(true)
    expect(user.is_user?).to eq(false)

  end

  it "can check user role is user" do

    user = FactoryBot.create(:user, role: 'user')

    expect(user.is_admin?).to eq(false)
    expect(user.is_super_admin?).to eq(false)
    expect(user.is_user?).to eq(true)

  end

  it "can't have a long password" do

    user = User.new(
      first_name: "A",
      last_name: "A",
      email: 'noreply@localhost.com',
      password: Array.new(73){ 'a' }.join)

    expected_validation_errors = {
      :password => [
        "is too long (maximum is 72 characters)",
        "length must be between 6 and 72 characters long",
        "must contain a number (0-9)",
        "must contain a symbol (!@$ etc)"
      ]
    }

    expect(user).to_not be_valid

    expect(user.errors.messages).to eq expected_validation_errors

  end

  it "can't have an empty password" do

    expect{FactoryBot.create(:user, email: 'noreply@localhost.com', password: '')}.to raise_error(ActiveRecord::RecordInvalid)

    user = User.new(
      first_name: "A",
      last_name: "A",
      email: 'noreply@localhost.com',
      password: '')

    expected_validation_errors = {
      :password => [
        "can't be blank",
        "length must be between 6 and 72 characters long",
        "must contain a number (0-9)",
        "must contain a symbol (!@$ etc)"
      ]
    }

    expect(user).to_not be_valid

    expect(user.errors.messages).to eq expected_validation_errors

  end

  it "can't have an invalid email" do

    expect{FactoryBot.create(:user, email: 'invalid_email')}.to raise_error(ActiveRecord::RecordInvalid)

  end

  it "can't have duplicate emails" do

    FactoryBot.create(:user, email: 'noreply@localhost.com')
    expect{FactoryBot.create(:user, email: 'NOREPLY@LOCALHOST.com')}.to raise_error(ActiveRecord::RecordInvalid)

  end

  it "forces email to be lowercase" do

    FactoryBot.create(:user, email: 'NOREPLY@localhost.com')
    expect(User.first.email).to eq('noreply@localhost.com')

  end

  it "must have first_name and last_name" do

    user = User.new(
      email: 'noreply@localhost.com',
      password: 'MyP@ssword11')

    expected_validation_errors = {
      :first_name => ["is too short (minimum is 1 character)"],
      :last_name => ["is too short (minimum is 1 character)"]
    }

    expect(user).to_not be_valid

    expect(user.errors.messages).to eq expected_validation_errors

  end

  it "must have a valid email/password" do

    user = User.new(
      first_name: "A",
      last_name: "A",
      email: 'noreply@localhost.com',
      password: 'MyP@ssword11')

    expect(user).to be_valid

  end

  it "raises an error if role is not recognized" do

    expect{FactoryBot.create(:user, role: 'ZZZZZ')}.to raise_error(RuntimeError)

  end

end
