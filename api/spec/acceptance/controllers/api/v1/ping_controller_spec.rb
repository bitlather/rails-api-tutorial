require 'rails_helper'
require 'rspec_api_documentation/dsl'

resource 'Ping' do

  explanation "Simple API requests on the Ping resource"

  header "Content-Type", "application/json"

  get '/api/v1/ping' do
    context '200' do
      example_request 'Getting a ping' do

        expect(status).to eq(200)

        expected_response = {
          "data" => { "ping" => "pong" }
          # Notice the arrows; the colons in the following fail:
          # "data": { "ping": "pong" }
        }
 
        expect(JSON.parse(response_body)).to eq(expected_response)
      end
    end
  end
end
